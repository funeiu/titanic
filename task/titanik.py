import pandas as pd
import numpy as np
'test commit'
def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()

    prefix = ['Mr', 'Mrs', 'Miss']

    def extract_prefix(name):
        data_prefix = name.split(',')[1].split('.')[0].strip()
        return data_prefix + "." if data_prefix in prefix else "NaN"
    
    df['Title'] = df['Name'].apply(extract_prefix)

    nan_count = df.groupby('Title')['Age'].apply(lambda x: x.isin([0, np.nan]).sum()).to_dict()

    median_pr = df.groupby('Title')['Age'].median().round().to_dict()

    result = [(data_prefix, nan_count[data_prefix], int(median_pr[data_prefix])) for data_prefix in ('Mr.', 'Mrs.', 'Miss.')]
    
    return result
